package ajtech.sdsmdg.cloop_assignment;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    static ArrayList<String> tab1, tab2, tab3;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    private ViewPager mViewPager;
    private static final int SELECTED_PICTURE = 1;
    int current_position =0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView = (ImageView) findViewById(R.id.imgView);
        tab1 = new ArrayList<String>();
        tab2 = new ArrayList<String>();
        tab3 = new ArrayList<String>();
        SharedPreferences prefs=this.getSharedPreferences("yourPrefsKey",Context.MODE_PRIVATE);
        Set<String> set1 = prefs.getStringSet("tab1", null);
        Set<String> set2 = prefs.getStringSet("tab2", null);
        Set<String> set3 = prefs.getStringSet("tab3", null);
        current_position = Integer.parseInt(prefs.getString("tabIndex","0"));
        try {
            tab1 = new ArrayList<String>(set1);
        }catch (NullPointerException exc){
            exc.printStackTrace();
        }
        try {
            tab2 = new ArrayList<String>(set2);
        }catch (NullPointerException exc){
            exc.printStackTrace();
        }
        try {
            tab3 = new ArrayList<String>(set3);
        }catch (NullPointerException exc){
            exc.printStackTrace();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager.setCurrentItem(current_position);
        Log.e("Current pos",current_position+" --");
        tabLayout.setupWithViewPager(mViewPager);


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                current_position = tab.getPosition();
                SharedPreferences prefs= getSharedPreferences("yourPrefsKey",Context.MODE_PRIVATE);
                SharedPreferences.Editor edit=prefs.edit();
                edit.putString("tabIndex",Integer.toString(current_position));
                edit.commit();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //i.putExtra("number",current_position);
                startActivityForResult(i, SELECTED_PICTURE);
                Log.e("Number :", current_position + " ");
                Log.e("size :", tab1.size() + " ");


            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case SELECTED_PICTURE:
                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    String[] projection = {MediaStore.Images.Media.DATA};

                    Cursor c = getContentResolver().query(uri, projection, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(projection[0]);
                    String filepath = c.getString(columnIndex);
                    c.close();

//                    Bitmap selectedImage = BitmapFactory.decodeFile(filepath);
//                    Drawable d = new BitmapDrawable(selectedImage);
                    Log.e("PATH", filepath);
                    switch (current_position) {
                        case 0:
                            tab1.add(filepath);
                            break;

                        case 1:
                            tab2.add(filepath);
                            break;


                        case 2:
                            tab3.add(filepath);
                            break;

                    }

                    SharedPreferences prefs=this.getSharedPreferences("yourPrefsKey",Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit=prefs.edit();

                    Set<String> set1 = new HashSet<String>();
                    set1.addAll(tab1);
                    edit.putStringSet("tab1", set1);
                    Set<String> set2 = new HashSet<String>();
                    set2.addAll(tab2);
                    edit.putStringSet("tab2", set2);
                    Set<String> set3 = new HashSet<String>();
                    set3.addAll(tab3);
                    edit.putStringSet("tab3", set3);
                    edit.commit();

                }
                break;
        }
        Intent i = new Intent(MainActivity.this,MainActivity.class);
        startActivity(i);
        finish();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        @Override
        public void onResume() {
            super.onResume();

        }

        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            Log.e("SSSNN", sectionNumber + " ");
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            Log.e("ascn", getArguments().getInt(ARG_SECTION_NUMBER) + "");
            int i = getArguments().getInt(ARG_SECTION_NUMBER);
            GridView gridView = (GridView) rootView.findViewById(R.id.gridview);
            gridView.setAdapter(new GridAdapter(i));
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));


            return rootView;
        }

        public class GridAdapter extends BaseAdapter {
            int pos = 1;

            public GridAdapter(int i) {
                super();
                pos = i;
            }

            @Override
            public int getCount() {
                if (pos == 1) {
                    return tab1.size();

                } else if (pos == 2) {
                    return tab2.size();
                } else return tab3.size();
            }

            @Override
            public Object getItem(int position) {
                if (pos == 1) {
                    return tab1.get(position);

                } else if (pos == 2) {
                    return tab2.get(position);
                } else return tab3.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                convertView = vi.inflate(R.layout.img_grid_layout, parent, false);
                ImageView imageView = (ImageView) convertView.findViewById(R.id.imgView);
//                imageView.setImageURI(Uri.parse(getItem(position).toString()));
                if (getItem(position).toString() != null) {
                    Bitmap bitmap = BitmapFactory.decodeFile(getItem(position).toString());
                    //code for compressing the img;
//                    ByteArrayOutputStream out = new ByteArrayOutputStream();
//                    bitmap.compress(Bitmap.CompressFormat.PNG, 70, out);
//                    Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
//                    imageView.setImageBitmap(decoded);
                    imageView.setImageBitmap(bitmap);
                } else {
                    imageView.setImageDrawable(getResources().getDrawable(R.mipmap.ic_launcher));
                }

                return convertView;
            }
        }

    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "MY PICS";
                case 1:
                    return "FAMILY";
                case 2:
                    return "OTHERS";
            }
            return null;
        }
    }
}
